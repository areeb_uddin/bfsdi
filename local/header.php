<!-- <div class="page-inner"> -->
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="index.php">BFSI Data Insights <em>.</em></a></div>
				</div>
				<div class="col-xs-4 menu-1">
					<ul>
						<li class="has-dropdown">
							<a href="#">Data <span style="font-size:10px; margin-left:3px; margin-bottom:1px;">&or;</span></a>
							<ul class="dropdown">
								<div class="row">
									<div class="col-sm-6">
										<ul class="">
											<li class="dropdown-header">Institutions</li>
											<li><a href="#">Banks</a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<ul class="">
											<li class="dropdown-header">Codes</li>
											<li><a href="ifsc.php">IFSC</a></li>
										</ul>
									</div>
								</div>
							</ul>
						</li>
						<li class="has-dropdown">
							<a href="#">Insights <span style="font-size:10px; margin-left:3px; margin-bottom:3px;">&or;</span></a>
							<ul class="dropdown">
								<li><a href="insights.php">IFSC INSIGHTS</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="search-box text-right">
					
					<form method="get" action="/search" id="search">
						<input name="q" type="text" class="searchtext" size="40" placeholder="Search..." />
					</form>	
					
				</div>
			</div>
			
		</div>
	</nav>