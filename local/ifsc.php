<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>IFSC</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by GetTemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="GetTemplates.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/bootstrap-datepicker.min.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

	
	<?php include("header.php"); ?>
	
	 <header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(images/cyprus-3184019_1920.jpg)">
		
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left" style="margin-left:-15%;">
					
					<div class="row row-mt-15em" style="margin-right:-13%;">
						
						<div class="col-md-12 col-md-push-1 animate-box" data-animate-effect="fadeInRight">
						
							<div class="limiter">
								<div class="wrap-login100">
									<span class="login100-form-title">IFSC Search</span>
									<form class="login100-form validate-form" autocomplete="off" method="post" action="ifsc.php#search-results">
										<div class="row">
											<div class="wrap-input100 validate-input" data-validate = "Select Bank">
												<input class="input100" type="text" name="bank" id="bank"/>		
												<span class="focus-input100" data-placeholder="Bank"></span>	
												<div id="myDropdown1" class="dropdown-content">
													<?php 
																require('config.php');
																
																$sql = "SELECT * FROM bank"; 

																$result = $mysqli->query($sql);
																
																if ($result->num_rows > 0) {
																	while($row = $result->fetch_assoc()){
																		$bankname = $row["BANK"];
																		$bankid = $row["BANK_ID"];
													?>
																	
																<li id="<?php echo $bankid; ?>" class="myLib" role="button"><?php echo $bankname; ?></li>
											
													<?php
																
																	}
																}

													?>
												</div>												
											</div>
											
											<div class="wrap-input100" data-validate = "Select State">
												<input class="input100" type="text" name="state" id="state"/>
												<span class="focus-input100" data-placeholder="State"></span>
												<div id="myDropdown2" class="dropdown-content">	
												</div>
											</div>
											<div class="wrap-input100" data-validate = "Select District">
												<input class="input100" type="text" name="district" id="district"/>
												<span class="focus-input100" data-placeholder="District"></span>
												<div id="myDropdown3" class="dropdown-content">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="wrap-input100 col-md-4" data-validate = "Select City">
												<input class="input100" type="text" name="city" id="city"/>
												<span class="focus-input100" data-placeholder="City"></span>
												<div id="myDropdown4" class="dropdown-content">
												</div>
											</div>
											<div class="wrap-input100 col-md-4" data-validate = "Select Branch">
												<input class="input100" type="text" name="branch" id="branch"/>
												<span class="focus-input100" data-placeholder="Branch"></span>
												<div id="myDropdown5" class="dropdown-content">	
												</div>
											</div>
											<div class="container-login100-form-btn col-md-4">
												<div class="wrap-login100-form-btn">
													<div class="login100-form-bgbtn"></div>
													<button class="login100-form-btn" type="submit" name="submit">
														Search
													</button>
												</div>
											</div>
										</div>
										<input type="hidden" name="bank_id" id="bank_id"/>
										<input type="hidden" name="state_id" id="state_id"/>
										<input type="hidden" name="district_id" id="district_id"/>
										<input type="hidden" name="city_id" id="city_id"/>
										<input type="hidden" name="branch_id" id="branch_id"/>
									</form>
								</div>
							</div>
						</div>
					</div>
							
					
				</div>
			</div>
		</div>
	</header>
	
	<div class="gtco-section" id="extraspace"></div>
	
<?php 

	if(isset($_POST['submit']))
	{
		$bank = $_POST['bank_id'];
		$state = $_POST['state_id'];
		$district = $_POST['district_id'];
		$city = $_POST['city_id'];
		$branch = $_POST['branch_id'];
		
		if($state == NULL && $district == NULL && $city == NULL && $branch == NULL)
		{
			$sql="select bankmaster.IFSC,bankmaster.MICRCODE,bankmaster.ADDRESS,bankmaster.CONTACT,bank.BANK,branch.BRANCH_NAME,cities.CITY,
					districts.DISTRICT,states.STATE from bankmaster inner join bank on bank.BANK_ID=bankmaster.BANK_ID
						inner join branch on branch.BRANCH_ID=bankmaster.BRANCH_ID inner join cities on cities.CITY_ID=bankmaster.CITY_ID
						inner join districts on districts.DISTRICT_ID=bankmaster.DISTRICT_ID inner join states on states.STATE_ID=bankmaster.STATE_ID
								where bankmaster.BANK_ID='".$bank."' ;";
	
		}
		else if($district == NULL && $city == NULL && $branch == NULL)
		{
			$sql="select bankmaster.IFSC,bankmaster.MICRCODE,bankmaster.ADDRESS,bankmaster.CONTACT,bank.BANK,branch.BRANCH_NAME,cities.CITY,
					districts.DISTRICT,states.STATE from bankmaster inner join bank on bank.BANK_ID=bankmaster.BANK_ID
						inner join branch on branch.BRANCH_ID=bankmaster.BRANCH_ID inner join cities on cities.CITY_ID=bankmaster.CITY_ID
						inner join districts on districts.DISTRICT_ID=bankmaster.DISTRICT_ID inner join states on states.STATE_ID=bankmaster.STATE_ID
								where bankmaster.BANK_ID='".$bank."' and bankmaster.STATE_ID='".$state."' ;";
		}
		else if($city == NULL && $branch == NULL)
		{
			$sql="select bankmaster.IFSC,bankmaster.MICRCODE,bankmaster.ADDRESS,bankmaster.CONTACT,bank.BANK,branch.BRANCH_NAME,cities.CITY,
						districts.DISTRICT,states.STATE from bankmaster
							inner join bank on bank.BANK_ID=bankmaster.BANK_ID inner join branch on branch.BRANCH_ID=bankmaster.BRANCH_ID
							inner join cities on cities.CITY_ID=bankmaster.CITY_ID inner join districts on districts.DISTRICT_ID=bankmaster.DISTRICT_ID
							inner join states on states.STATE_ID=bankmaster.STATE_ID
							where bankmaster.BANK_ID='".$bank."' and bankmaster.STATE_ID='".$state."' and bankmaster.DISTRICT_ID='".$district."';";
		}
		else if($branch == NULL)
		{
			$sql="select bankmaster.IFSC,bankmaster.MICRCODE,bankmaster.ADDRESS,bankmaster.CONTACT,bank.BANK,branch.BRANCH_NAME,cities.CITY,
					districts.DISTRICT,states.STATE from bankmaster inner join bank on bank.BANK_ID=bankmaster.BANK_ID
						inner join branch on branch.BRANCH_ID=bankmaster.BRANCH_ID inner join cities on cities.CITY_ID=bankmaster.CITY_ID
						inner join districts on districts.DISTRICT_ID=bankmaster.DISTRICT_ID inner join states on states.STATE_ID=bankmaster.STATE_ID
						where bankmaster.BANK_ID='".$bank."' and bankmaster.STATE_ID='".$state."' and bankmaster.DISTRICT_ID='".$district."' 
															and bankmaster.CITY_ID='".$city."';";
		}
		else
		{
			
			$sql="select bankmaster.IFSC,bankmaster.MICRCODE,bankmaster.ADDRESS,bankmaster.CONTACT,bank.BANK,branch.BRANCH_NAME,cities.CITY,
					districts.DISTRICT,states.STATE from bankmaster inner join bank on bank.BANK_ID=bankmaster.BANK_ID
						inner join branch on branch.BRANCH_ID=bankmaster.BRANCH_ID inner join cities on cities.CITY_ID=bankmaster.CITY_ID
						inner join districts on districts.DISTRICT_ID=bankmaster.DISTRICT_ID inner join states on states.STATE_ID=bankmaster.STATE_ID
								where bankmaster.BANK_ID='".$bank."' and bankmaster.BRANCH_ID='".$branch."' and bankmaster.CITY_ID='".$city."' 
								and bankmaster.DISTRICT_ID='".$district."' and bankmaster.STATE_ID='".$state."' ";
								
		}
		
		$result = mysqli_query($mysqli, $sql);
		
?>
	<div class="gtco-section" id="search-results" style="padding-left:3%; padding-right:3%;">
		<div class="bg-light-gray search-wrap" id="card-paginate">
			<div class="search-header">
				<span></span>
				<h3>Search Results</h3>
				<div class="wrap">
				   <div class="search">
					  <input type="text" class="searchTerm" placeholder="Customize Search Results">
					  <button type="submit" class="searchButton" disabled>
						<i class="fa fa-search"></i>
					 </button>
				   </div>
				</div>
			</div>
<?php 
		$count = 0;
		$c = 1;
		while($row = mysqli_fetch_array($result)) 
		{
			$count++;
			if($count > 5)
			{
				$c++;
				$count = 1;
			}
?>
		<div class="gtco-container card-container" id="c<?php echo $c.$count; ?>">
			<div class="row">
				<div class="textblock1 col-md-10 col-sm-10">
					<h4><?php echo $row['BANK']; ?></h4>
					<h6 class="text-muted">Branch : <?php echo $row['BRANCH_NAME']; ?><h6>
					<p><?php echo $row['ADDRESS']; ?></p>
					<a href="#">View Details</a>
				</div> 
				<div class="textblock2 col-md-2 col-sm-2">
					<div class="row text1">
						<span>City :<span>
						<span><?php echo $row['CITY']; ?><span>
					</div>
					<div class="row text1">
						<span>State :<span>
						<span><?php echo $row['STATE']; ?><span>
					</div>
					<div class="row text1">
						<span>IFSC Code :<span>
						<span><?php echo $row['IFSC']; ?><span>
					</div>
					<div class="row text1">
						<span>MICR Code :<span>
						<span><?php echo $row['MICRCODE']; ?><span>
					</div>
					<div class="row text1">
						<span>Contact :<span>
						<span><?php echo $row['CONTACT']; ?><span>
					</div>
				</div>
			</div>
		</div>
<?php
			if($c > 1)
			{
				echo "<script>$('#c".$c.$count."').addClass('hide');</script>";
			}
		}
?>
		<nav role="navigation" class="cd-navigation">
			<ul class="cd-pagination" id="cd-pagination">
				<li class="button" id="prev" role="button"><a class="disabled">Prev</a></li>
				<li role="button" id="l1" class="num current"><a>1</a></li>
<?Php 		
			$m=2;
			while($m <= $c)
			{
				if($m > 5)
				{
					echo "<li role='button' class='num hide' id='l".$m."'><a>".$m."</a></li>";
					$m++;
				}
				else
				{
					echo "<li role='button' class='num' id='l".$m."'><a>".$m."</a></li>";
					$m++;
				}
			}
?>
				<li class="button" role="button" id="next"><a class="disabled">Next</a></li>
				
<?php 
			if($c > 1)
			{
				echo "<script>$('#next a').removeClass('disabled');</script>";
			}
?>
			</ul>
		</nav>
		</div>
	</div>
	
	<script>
		$(document).ready(function(){
			var links = document.getElementsByClassName('num');
			var lin;
		 
			for(var i=0; i<=links.length ;i++)
			{
				lin = links[i];
				if(lin)
				{
					lin.onclick = dynamicevent;
				}
			}
		
			function dynamicevent (){
				var id = this.id;
				id = id.substr(1);
				var cid = $('.current').attr('id');				
				$(this).siblings().removeClass('current');
				$(this).addClass('current');
				for(var i=1;i<=5;i++)
				{
					$('#c'+id+i+'').removeClass('hide');
					
					for(var j=1;j<id;j++)
					{
						$('#c'+j+i+'').addClass('hide');
					}
					var k = <?php echo $c; ?>;
					var m = parseFloat(id) + 1;
					m = m.toString();
					for(var j=m;j<=k;j++)
					{
						$('#c'+j+i+'').addClass('hide');
					}
				}
				if(id > 1)
				{
					$('#prev a').removeClass('disabled');
				}
				else
				{
					$('#prev a').addClass('disabled');
				}
				if(id < k)
				{
					$('#next a').removeClass('disabled');
				}
				else
				{
					$('#next a').addClass('disabled');
				}
				
				var id3 = id;
				id3 = parseFloat(id3);
				id3 = id3 - 1;
				
				var id2 = id;
				id2 = parseFloat(id2);
				id2 = id2 + 1;
				
				if($('#l'+id3+'').hasClass('hide'))
				{
					$('#l'+id3+'').removeClass('hide');
					for(var i=1;i<4;i++)
					{
						id3 = id3 - 1;
						$('#l'+id3+'').removeClass('hide');
					}
					id3 = id;
					id3 = parseFloat(id3);
					for(var i=0;i<4;i++)
					{
						id3 = id3 + 1;
						$('#l'+id3+'').addClass('hide');
					}
				}
				
				
				else if($('#l'+id2+'').hasClass('hide'))
				{
					$('#l'+id2+'').removeClass('hide');
					for(var i=1;i<4;i++)
					{
						id2 = id2 + 1;
						$('#l'+id2+'').removeClass('hide');
					}
					id2 = id;
					id2 = parseFloat(id2);
					for(var i=0;i<4;i++)
					{
						id2 = id2 - 1;
						$('#l'+id2+'').addClass('hide');
					}
				}
				location.href= "#card-paginate";
			}
			
			$('#prev').click(function(){
				var lid = $('.current').attr('id');
				lid = lid.substr(1);
				lid = parseFloat(lid) - 1;
				lid = lid.toString();
				$('#l'+lid+'').trigger('click');
			});
			
			$('#next').click(function(){
				var lid = $('.current').attr('id');
				lid = lid.substr(1);
				lid = parseFloat(lid) + 1;
				lid = lid.toString();
				$('#l'+lid+'').trigger('click');
			});
		});
	</script>
	<script>
		$(document).ready(function(){
		  $('.searchTerm').on('keyup', function() {
			var value = $(this).val().toLowerCase();
			$('.card-container').filter(function() {
			  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		  });
		});
	</script>
<?php
		echo "<script>$('#extraspace').addClass('hide');</script>";
	}
?>
	
	<?php include("footer.php"); ?>

	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>

	<script>
	
	$(document).ready(function(){
		
		var bankid, stateid, districtid, cityid, branchid, hoverid;
		
		/** Showing the dropdown on clicking the fields **/
		$('#bank').click(function(event){
			$('#myDropdown1').toggleClass("show", true);
			$('#myDropdown2').toggleClass("show", false);
			$('#myDropdown3').toggleClass("show", false);
			$('#myDropdown4').toggleClass("show", false);
			$('#myDropdown5').toggleClass("show", false);
			event.stopPropagation();
			//$('#myDropdown1 li').first().css("background-color", "#ddd");
			
					
		});
		
			/*$('#myDropdown1 li.myLib').hover(function() {
			   hoverid = this.id;	
			   console.log(hoverid);
			});
		
			
			$('#bank').keydown(function(event){
				//alert("hi");
				//$('#'+hoverid+'').css("background-color", "");
				if(event.keyCode === 38)
				{
					hoverid = parseInt(hoverid);
					hoverid = hoverid - 1;
					hoverid = hoverid.toString();
					console.log(hoverid);
					document.body.style.pointerEvents = 'none';
					$('#'+hoverid+'').css('background-color', '#ddd');
				}
				if(event.keyCode === 40)
				{
					hoverid = parseInt(hoverid);
					hoverid = hoverid + 1;
					hoverid = hoverid.toString();
					console.log(hoverid);
					$('#'+hoverid+'').css('background-color', '#ddd');
					document.body.style.pointerEvents = 'none';
				}
			});
			
			$(document).mousemove(function(){
				document.body.style.pointerEvents = 'auto';
				$('#myDropdown1 li.myLib').trigger('hover');
			});*/
		
		$('#state').click(function(event){
			$('#myDropdown1').toggleClass("show", false);
			$('#myDropdown2').toggleClass("show", true);
			$('#myDropdown3').toggleClass("show", false);
			$('#myDropdown4').toggleClass("show", false);
			$('#myDropdown5').toggleClass("show", false);
			event.stopPropagation();
		});
		$('#district').click(function(event){
			$('#myDropdown1').toggleClass("show", false);
			$('#myDropdown2').toggleClass("show", false);
			$('#myDropdown3').toggleClass("show", true);
			$('#myDropdown4').toggleClass("show", false);
			$('#myDropdown5').toggleClass("show", false);
			event.stopPropagation();
		});
		$('#city').click(function(event){
			$('#myDropdown1').toggleClass("show", false);
			$('#myDropdown2').toggleClass("show", false);
			$('#myDropdown3').toggleClass("show", false);
			$('#myDropdown4').toggleClass("show", true);
			$('#myDropdown5').toggleClass("show", false);
			event.stopPropagation();
		});
		$('#branch').click(function(event){
			$('#myDropdown1').toggleClass("show", false);
			$('#myDropdown2').toggleClass("show", false);
			$('#myDropdown3').toggleClass("show", false);
			$('#myDropdown4').toggleClass("show", false);
			$('#myDropdown5').toggleClass("show", true);
			event.stopPropagation();
		});
		
		$(document).click(function(){
			$('#myDropdown1').toggleClass("show", false);
			$('#myDropdown2').toggleClass("show", false);
			$('#myDropdown3').toggleClass("show", false);
			$('#myDropdown4').toggleClass("show", false);
			$('#myDropdown5').toggleClass("show", false);
		});
		
		/** Getting dropdown values from database through ajax calls **/
		
		$('#myDropdown1').on('click', 'li.myLib', function(){
			var bankname = $(this).text();
			bankid = this.id;
			$('#bank').val(bankname).trigger('change');
			$('#bank_id').val(bankid);
			$('.input100').each(function(){
				
				if($(this).val().trim() != "") {
					$(this).addClass('has-val');
				}
				else {
					$(this).removeClass('has-val');
				}
			 
			});
			
			$.ajax({
					url: "ajax1.php",
					dataType: 'Json',
					data: {'id':bankid},
					success: function(data) {
						$('#myDropdown2').empty();
						$.each(data, function(key, value) {
							$('#myDropdown2').append('<li id='+key+' class="myLis" role="button">'+value+'</li>');
						});
					}
				});
				
		});
		
		$('#myDropdown2').on("click", "li.myLis", function(){
			var statename = $(this).text();
			stateid = this.id;
			$('#state').val(statename).trigger('change');
			$('#state_id').val(stateid);
			$('.input100').each(function(){
				
				if($(this).val().trim() != "") {
					$(this).addClass('has-val');
				}
				else {
					$(this).removeClass('has-val');
				}
			 
			});
			
			$.ajax({
					url: "ajax2.php",
					dataType: 'Json',
					data: {'id':stateid, 'id2':bankid},
					success: function(data) {
						$('#myDropdown3').empty();
						$.each(data, function(key, value) {
							$('#myDropdown3').append('<li id='+key+' class="myLid" role="button">'+value+'</li>');
						});
					}
				});
			
		});
		
		$('#myDropdown3').on("click", "li.myLid", function(){
			var districtname = $(this).text();
			districtid = this.id;
			$('#district').val(districtname).trigger('change');
			$('#district_id').val(districtid);
			$('.input100').each(function(){
				
				if($(this).val().trim() != "") {
					$(this).addClass('has-val');
				}
				else {
					$(this).removeClass('has-val');
				}
			 
			});
			
			$.ajax({
					url: "ajax3.php",
					dataType: 'Json',
					data: {'id':stateid, 'id1':districtid, 'id2':bankid},
					success: function(data) {
						$('#myDropdown4').empty();
						$.each(data, function(key, value) {
							$('#myDropdown4').append('<li id='+key+' class="myLic" role="button">'+value+'</li>');
						});
					}
				});
			
		});
		
		$('#myDropdown4').on("click", "li.myLic", function(){
			var cityname = $(this).text();
			cityid = this.id;
			$('#city').val(cityname).trigger('change');
			$('#city_id').val(cityid);
			$('.input100').each(function(){
				
				if($(this).val().trim() != "") {
					$(this).addClass('has-val');
				}
				else {
					$(this).removeClass('has-val');
				}
			 
			});
			
			$.ajax({
					url: "ajax4.php",
					dataType: 'Json',
					data: {'id':stateid, 'id1':districtid, 'id2':cityid, 'id3':bankid},
					success: function(data) {
						$('#myDropdown5').empty();
						$.each(data, function(key, value) {
							$('#myDropdown5').append('<li id='+key+' class="myLibr" role="button">'+value+'</li>');
						});
					}
				});
			
		});
		
		$('#myDropdown5').on("click", "li.myLibr", function(){
			var branchname = $(this).text();
			branchid = this.id;
			$('#branch').val(branchname).trigger('change');
			$('#branch_id').val(branchid);
			$('.input100').each(function(){
				
				if($(this).val().trim() != "") {
					$(this).addClass('has-val');
				}
				else {
					$(this).removeClass('has-val');
				}
			 
			});
		});
		
		/** Make Text boxes empty on change in previous Text box **/
		
		$('#bank').change(function(){
			$('#state').val('');
			$('#district').val('');
			$('#city').val('');
			$('#branch').val('');
			$('#myDropdown2').empty();
			$('#myDropdown3').empty();
			$('#myDropdown4').empty();
			$('#myDropdown5').empty();
		});
		$('#state').change(function(){
			$('#district').val('');
			$('#city').val('');
			$('#branch').val('');
			$('#myDropdown3').empty();
			$('#myDropdown4').empty();
			$('#myDropdown5').empty();
		});
		$('#district').change(function(){
			$('#city').val('');
			$('#branch').val('');
			$('#myDropdown4').empty();
			$('#myDropdown5').empty();
		});
		$('#city').change(function(){
			$('#branch').val('');
			$('#myDropdown5').empty();
		});
		
		/** Dropdown changes based on key input **/
		
		$('#bank').keyup(function(){
			var searchtext = $(this).val();
			$.ajax({
					url: "ajax0.php",
					dataType: 'Json',
					data: {'val': searchtext},
					success: function(data) {
						$('#myDropdown1').empty();
						$.each(data, function(key, value) {
							$('#myDropdown1').append('<li id='+key+' class="myLib" role="button">'+value+'</li>');
						});
					}
				});
		});
		
		$('#state').keyup(function(){
			var searchtext = $(this).val();
			$.ajax({
					url: "ajax1.php",
					dataType: 'Json',
					data: {'id':bankid, 'val': searchtext},
					success: function(data) {
						$('#myDropdown2').empty();
						$.each(data, function(key, value) {
							$('#myDropdown2').append('<li id='+key+' class="myLis" role="button">'+value+'</li>');
						});
					}
				});
		});
		
		$('#district').keyup(function(){
			var searchtext = $(this).val();
			$.ajax({
					url: "ajax2.php",
					dataType: 'Json',
					data: {'id':stateid, 'id2':bankid, 'val': searchtext},
					success: function(data) {
						$('#myDropdown3').empty();
						$.each(data, function(key, value) {
							$('#myDropdown3').append('<li id='+key+' class="myLid" role="button">'+value+'</li>');
						});
					}
				});
		});
		
		$('#city').keyup(function(){
			var searchtext = $(this).val();
			$.ajax({
					url: "ajax3.php",
					dataType: 'Json',
					data: {'id':stateid, 'id1':districtid, 'id2':bankid, 'val': searchtext},
					success: function(data) {
						$('#myDropdown4').empty();
						$.each(data, function(key, value) {
							$('#myDropdown4').append('<li id='+key+' class="myLic" role="button">'+value+'</li>');
						});
					}
				});
		});
		
		$('#branch').keyup(function(){
			var searchtext = $(this).val();
			$.ajax({
					url: "ajax4.php",
					dataType: 'Json',
					data: {'id':stateid, 'id1':districtid, 'id2':cityid, 'id3':bankid, 'val': searchtext},
					success: function(data) {
						$('#myDropdown5').empty();
						$.each(data, function(key, value) {
							$('#myDropdown5').append('<li id='+key+' class="myLibr" role="button">'+value+'</li>');
						});
					}
				});
		});
		
	});
	</script>
	
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	
	<!-- Datepicker -->
	<script src="js/bootstrap-datepicker.min.js"></script>
	
	<!-- Main -->
	<script src="js/main.js"></script>
	
	

	</body>
</html>

